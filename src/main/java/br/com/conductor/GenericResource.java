/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor;

import br.com.conductor.dao.PessoaDAO;
import br.com.conductor.model.Pessoa;
import br.com.conductor.standards.DaoResponse;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author WILLIAM
 */
@Path("banco")
public class GenericResource
{

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource()
    {
    }

    /**
     * Retrieves representation of an instance of
     * br.com.conductor.GenericResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson()
    {

        return "webservice RESTFUL";
        //TODO return proper representation object
        //throw new UnsupportedOperationExceptiogetPn();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("Pessoa/inserir/{nome}/{sobrenome}/{email}")
    public String inserirPessoa(
            @PathParam("nome") String nome,
            @PathParam("sobrenome") String sobrenome,
            @PathParam("email") String email)
    {

        boolean nomeVazio = nome.equals("{nome}");
        boolean sobrenomeVazio = sobrenome.equals("{sobrenome}");
        boolean emailVazio = email.equals("{email}");

        String retornoCliente = DaoResponse.RESPONSE_DEFAULT;
        if ((!nomeVazio) && (!sobrenomeVazio) && (!emailVazio))
        {
            Pessoa p = new Pessoa();
            p.setNome(nome);
            p.setSobrenome(sobrenome);
            p.setEmail(email);
            PessoaDAO pd = new PessoaDAO();
            String retornoDAO = pd.inserirPessoa(p);

            switch (retornoDAO)
            {
                case DaoResponse.RESPONSE_OK:
                    Gson g = new Gson();
                    String json = g.toJson(p);
                    retornoCliente = "Pessoa inserida com sucesso! Pessoa: " + json;
                    break;

                default:
                    retornoCliente = "Não foi possível inserir a pessoa" + nome + ", " + retornoDAO;
                    break;
            }

        }
        else
        {
            retornoCliente = "Por favor informe nome, sobrenome e email da pessoa.";
        }
        //retorno para o usuario 
        return retornoCliente;
    }

    /**
     *
     * @param id
     * @param nome
     * @param sobrenome
     * @param email
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("Pessoa/atualizar/{codigo}/{nome}/{sobrenome}/{email}")
    public String atualizarPessoa(@PathParam("codigo") Long id, @PathParam("nome") String nome, @PathParam("sobrenome") String sobrenome, @PathParam("email") String email)
    {
        boolean idVazio = nome.equals("{codigo}");
        boolean nomeVazio = nome.equals("{nome}");
        boolean sobrenomeVazio = sobrenome.equals("{sobrenome}");
        boolean emailVazio = email.equals("{email}");
        String retornoCliente = DaoResponse.RESPONSE_DEFAULT;

        if ((!nomeVazio) && (!sobrenomeVazio) && (!emailVazio) && (!idVazio))
        {
            Pessoa p = new Pessoa();
            p.setNome(nome);
            p.setSobrenome(sobrenome);
            p.setEmail(email);
            p.setId(id);
            PessoaDAO pd = new PessoaDAO();
            String retornoDAO = pd.atualizar(p);

            switch (retornoDAO)
            {
                case DaoResponse.RESPONSE_OK:
                    Gson g = new Gson();
                    String json = g.toJson(p);
                    retornoCliente = "Pessoa Atualizada com sucesso! Pessoa: " + json;
                    break;
                case DaoResponse.RESPONSE_NOT_FOUND:
                    retornoCliente = "a pessoa de nome " + id + " não existe, verifique o nome e tente novamente.";
                    break;
                default:
                    retornoCliente = "Não foi possível atualizar a pessoa" + nome + ", " + retornoDAO;
                    break;
            }
        }
        else
        {
            retornoCliente = "Por favor informe o id, nome, sobrenome e email da pessoa para ser atualizado.";
        }
        //retorno para o usuario 
        return retornoCliente;
    }

    /**
     *
     * @param nome
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("Pessoa/excluir/{nome}")
    public String excluirPessoa(@PathParam("nome") String nome)
    {
        String retornoCliente = DaoResponse.RESPONSE_DEFAULT;

        //vazio
        if (!nome.equals("{nome}"))
        {
            Pessoa p = new Pessoa();
            p.setNome(nome);
            PessoaDAO pd = new PessoaDAO();
            String retornoDAO = pd.excluir(p);

            switch (retornoDAO)
            {
                case DaoResponse.RESPONSE_OK:
                    Gson g = new Gson();
                    String json = g.toJson(p);
                    retornoCliente = "Pessoa " + nome + " removida com sucesso!";
                    break;
                case DaoResponse.RESPONSE_NOT_FOUND:
                    retornoCliente = "a pessoa de nome " + nome + " não existe, verifique o nome e tente novamente.";
                    break;
                default:
                    retornoCliente = "Não foi possível remover a pessoa" + nome + ", " + retornoDAO;
                    break;
            }
        }
        else
        {
            retornoCliente = "Por favor, informe um nome para ser excluido.";
        }
        return retornoCliente;
    }

    /**
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("Pessoa/List")
    public String listarPessoas()
    {
        String retornoCliente = DaoResponse.RESPONSE_DEFAULT;
        List<Pessoa> lista = new ArrayList<>();
        PessoaDAO pd = new PessoaDAO();
        lista = pd.listar();
        Gson g = new Gson();
        retornoCliente = g.toJson(lista);
        return retornoCliente;
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content)
    {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("Pessoa/get/{nome}")
    public String buscaPessoa(@PathParam("nome") String nome)
    {
        String retornoCliente = DaoResponse.RESPONSE_DEFAULT;

        if (!nome.equals("{nome}"))
        {
            Pessoa p = new Pessoa();
            p.setNome(nome);
            PessoaDAO pd = new PessoaDAO();
            Map<Pessoa, String> retornoDAO = pd.buscar(p);
            Map.Entry<Pessoa, String> no = retornoDAO.entrySet().iterator().next();
            Pessoa pessoa = no.getKey();
            String mRetornoDAO = no.getValue();

            switch (mRetornoDAO)
            {
                case DaoResponse.RESPONSE_OK:
                    Gson g = new Gson();
                    String json = g.toJson(p);
                    retornoCliente = "Dados da Pessoa: " + json;
                    break;
                case DaoResponse.RESPONSE_NOT_FOUND:
                    retornoCliente = "a pessoa de nome " + nome + " não existe, verifique o nome e tente novamente.";
                    break;
                default:
                    retornoCliente = "Não foi possível buscar a pessoa" + nome + ", " + mRetornoDAO;
                    break;
            }
        }
        else
        {
            retornoCliente = "Por favor, informe um nome para realizar a busca.";
        }

        return retornoCliente;
    }
}
