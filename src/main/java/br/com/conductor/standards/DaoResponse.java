/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.standards;

public class DaoResponse {

    public static final String RESPONSE_DEFAULT = "n/a";
    public static final String RESPONSE_OK = "ok";
    public static final String RESPONSE_ERROR = "erro";
    public static final String RESPONSE_NOT_FOUND= "não existe";
}
