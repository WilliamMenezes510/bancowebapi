/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.dao;

import br.com.conductor.model.Pessoa;
import br.com.conductor.standards.DaoResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WILLIAM
 */
public class PessoaDAO
{

    private Connection conexaoBase;
    private Connection conexaoBanco;
    private Statement consulta;
    private PreparedStatement consultaPreparada;
    //Singleton
    private final Conexao DAO = Conexao.getInstance();
    private static final Logger LOG = Logger.getLogger(PessoaDAO.class.getName());

    public PessoaDAO()
    {

    }

    /**
     *
     * @param pessoa
     * @return
     */
    public String inserirPessoa(Pessoa pessoa)
    {
        String sql = "INSERT INTO pessoa(nome,sobrenome,email) VALUES(?,?,?)";
        String retorno = DaoResponse.RESPONSE_DEFAULT;
        PreparedStatement pst = DAO.getPreparedStatement(sql);
        try
        {
            pst.setString(1, pessoa.getNome());
            pst.setString(2, pessoa.getSobrenome());
            pst.setString(3, pessoa.getEmail());

            if (pst.executeUpdate() > 0)
            {
                retorno = DaoResponse.RESPONSE_OK;
            }
            else
            {
                retorno = DaoResponse.RESPONSE_NOT_FOUND;
            }

        }
        catch (SQLException ex)
        {
            LOG.log(Level.SEVERE, null, ex);
            retorno = DaoResponse.RESPONSE_ERROR + ex.getLocalizedMessage();
        }

        return retorno;

    }

    public String atualizar(Pessoa pessoa)
    {
        String sql = "UPDATE pessoa set nome=?,sobrenome=?,email=? where id=?";

        String retorno = DaoResponse.RESPONSE_DEFAULT;
        PreparedStatement pst = DAO.getPreparedStatement(sql);
        try
        {

            pst.setString(1, pessoa.getNome());
            pst.setString(2, pessoa.getSobrenome());
            pst.setString(3, pessoa.getEmail());
            pst.setLong(4, pessoa.getId());
            if (pst.executeUpdate() > 0)
            {
                retorno = DaoResponse.RESPONSE_OK;
            }
            else
            {
                retorno = DaoResponse.RESPONSE_NOT_FOUND;
            }

        }
        catch (SQLException ex)
        {
            LOG.log(Level.SEVERE, null, ex);
            retorno = ex.getLocalizedMessage() + DaoResponse.RESPONSE_ERROR;
        }

        return retorno;

    }

    public String excluir(Pessoa pessoa)
    {
        String sql = "DELETE FROM pessoa where nome=?";
        String retorno = DaoResponse.RESPONSE_DEFAULT;
        PreparedStatement pst = DAO.getPreparedStatement(sql);
        try
        {

            pst.setString(1, pessoa.getNome());
            if (pst.executeUpdate() > 0)
            {
                retorno = DaoResponse.RESPONSE_OK;
            }
            else
            {
                retorno = DaoResponse.RESPONSE_NOT_FOUND;
            }

        }
        catch (SQLException ex)
        {
            LOG.log(Level.SEVERE, null, ex);
            retorno = ex.getLocalizedMessage() + DaoResponse.RESPONSE_ERROR;
        }

        return retorno;

    }

    public List<Pessoa> listar()
    {
        String sql = "SELECT * FROM pessoa";
        List<Pessoa> retorno = new ArrayList<>();
        PreparedStatement pst = DAO.getPreparedStatement(sql);
        try
        {

            ResultSet res = pst.executeQuery();
            while (res.next())
            {
                Pessoa item = new Pessoa();
                item.setId(res.getLong("id"));
                item.setNome(res.getString("nome"));
                item.setSobrenome(res.getString("sobrenome"));
                item.setEmail(res.getString("email"));
                retorno.add(item);

            }

        }
        catch (SQLException ex)
        {
            LOG.log(Level.SEVERE, null, ex);

        }

        return retorno;

    }

    public Map<Pessoa, String> buscar(Pessoa pessoa)
    {
        String sql = "SELECT * FROM pessoa where nome=?";
        Map<Pessoa, String> retorno = new HashMap();
        Pessoa p = new Pessoa();
        retorno.put(p, DaoResponse.RESPONSE_DEFAULT);

        PreparedStatement pst = DAO.getPreparedStatement(sql);
        try
        {

            pst.setString(1, pessoa.getNome());
            ResultSet res = pst.executeQuery();

            if (res.next())
            {

                p.setNome(res.getString("nome"));
                p.setSobrenome(res.getString("sobrenome"));
                p.setEmail(res.getString("email"));
                retorno.put(p, DaoResponse.RESPONSE_OK);
            }
            else
            {
                retorno.put(p, DaoResponse.RESPONSE_NOT_FOUND);
            }

        }
        catch (SQLException ex)
        {
            LOG.log(Level.SEVERE, null, ex);
            retorno.put(p, DaoResponse.RESPONSE_ERROR + ex.getLocalizedMessage());
        }

        return retorno;

    }
}
