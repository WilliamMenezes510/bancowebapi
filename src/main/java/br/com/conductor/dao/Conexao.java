/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.conductor.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author William
 */
public final class Conexao {

    
    
    private static Connection CONEXAO;
    private Statement consulta;
    private PreparedStatement consultaPreparada;
    private ResultSet resultado;
    private final String URLBASE = "jdbc:mysql://localhost:3306/conductor_db";
    private final String DRIVER = "com.mysql.jdbc.Driver";
    private final String USER = "root";
    private final String PASS = "root";

    
    private Conexao()
    {
        getConexao();
    }
   
    public PreparedStatement getPreparedStatement(String sql) {
        // testo se a conexão já foi criada
        if (CONEXAO == null) {
            // cria a conexão
            CONEXAO = getConexao();
        }
        try {
            // retorna um objeto java.sql.PreparedStatement
            return CONEXAO.prepareStatement(sql);
        } catch (SQLException e) {
            System.out.println("Erro de sql: "
                    + e.getMessage());
        }
        return null;
    }

   
    public boolean fechaConexoes(boolean connections, boolean consultas, boolean resultados) {
        try {
            if (connections) {
                if (CONEXAO != null) {
                    CONEXAO.close();
                } else if (CONEXAO != null) {
                    CONEXAO.close();
                }
            } else if (consultas) {
                if (consulta != null) {
                    consulta.close();
                } else if (consultaPreparada != null) {
                    consultaPreparada.close();
                }
            } else if (resultados) {
                if (resultado != null) {
                    resultado.close();
                }
            }
            return true;
        } catch (SQLException ex) {
            ex.getMessage();
            //ExceptionManager.ThrowException("Erro ao Fechar Conexoes", "", ex);
        }
        return false;
    }

  
    public Connection getConexao() {

        if (CONEXAO == null) {

            try//A captura de exceÃ§Ãµes SQLException em Java Ã© obrigatÃ³ria para usarmos JDBC.
            {

                // Este Ã© um dos meios para registrar um driver
                Class.forName(DRIVER).newInstance();
                CONEXAO = DriverManager.getConnection(URLBASE, USER, PASS);
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException ex) {
                print("erro: " + ex.getLocalizedMessage());
            }

        }
        return CONEXAO;

    }

    private static void print(String msg) {
        System.out.println(msg);
    }

    /**
     *
     * @return
     */
    public static Conexao getInstance() {
        return SingletonHolder.INSTANCE;
    }

   
    private static class SingletonHolder {

        protected static final Conexao INSTANCE = new Conexao();
    }
}
