Nome Projeto App: Banco Conductor
             Apo: BancoWS
-----------------------
Aplicaçao: 
API: REST - Java
Back End

Repositório dos Projetos:
Aplicação: https://bitbucket.org/WilliamMenezes510/bancowebapplication/commits/all
API REST: https://bitbucket.org/WilliamMenezes510/bancowebapi/src/master/	

Componentes Utilizados: 
------------------------------
JDK 1.8 < 
Maven 4.0 
JSF 2.2<br 
MySQL 5.0
JPA 2.5.2 
GlassFish 4.1 
PrimeFaces 5.0 
Netbeans 8.2<

Aplicação:
------------------
Escopo Front End:
100 % concluído.
Validações:
Tamanho de cada campo limitado a 50 caracteres
Valida o formato de e-mail 
obriga o preenchimento do nome 
obriga o preenchimento do e-mail
texto autoexplicativo em cada campo
Usabilidade:
Menus Recolhiveis
Icones Amigaveis
Interface Moderna
Menus e Botões intiuitivos 
Placeholders nos campos de input text
Validações para campos em branco
icons Jquery
icons Font Awesome

Escopo Back End (API):
----------------------
Schedule de encargos	-  não implementado devido a restrições de tempo
Banco de Dados Relacional	- OK
Modelar o Banco de Dados	OK
Cadastro de Pessoas :
<body>
        Listar<br>
        <a href="http://localhost:8080/BancoWS/webresources/banco/Pessoa/List">Listar</a><br><br>
        Insetir {nome}/{sobrenome}/{email}<br>
        <a href="http://localhost:8080/BancoWS/webresources/banco/Pessoa/inserir/">Inserir</a><br><br>
        Atualizar: {codigo}/{nome}/{sobrenome}/{email}<br>
        <a href="http://localhost:8080/BancoWS/webresources/banco/Pessoa/atualizar/">Atualizar</a><br><br>
        Buscar: {nome}<br>
        <a href="http://localhost:8080/BancoWS/webresources/banco/Pessoa/get/">Buscar</a><br><br>
        Delete: {nome}<br>
        <a href="http://localhost:8080/BancoWS/webresources/banco/Pessoa/excluir/">Deletar</a><br><br>
    </body>

Validações: valores em branco, valores inexistes, erro de execução.
Cadastro de Contas – não implementado devido a restrições de tempo
Transferência de Limites –
A Api retorna os dados em formato JSON.
	
Arquitetura:
---------------
Back End (App e API):
-Organizado controladores, visao e persistência, entidades em pacotes distintos

Como Executar:
---------
Aplicação:
1 - Importar o arquivo conductor_db.sql para o banco de Dados MySQL
2 - Importar os Projetos no Netbeans 8+
3 - Baixar o Glassfish 4.1
4 - Configurar um server glassfish no netbeans 
5 - realizar o build com as dependencias pelo maven 
6 - Realizar deploy

API:
1- importar o projeto BancoWS para o netbeans
2 - realizar buiid com dependencias pelo maven
3 - Configurar um server glassfish no netbeans 
4 - Realizar deploy
Acessar:
App:http://localhost:8080/BancoConductor/
App:http://localhost:8080/BancoWS